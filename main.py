import chain


t1 = "Transaktion: Alice sent 0.4 ITSecCoin zu Bob"
t2 = "Transaktion: Bob sent 0.2 ITSecCoin zu Tim"
t3 = "Transaktion: Seda sent 0.6 ITSecCoin zu Kim"
t4 = "Transaktion: Ziyu sent 1.2 ITSecCoin zu Markus"
t5 = "Transaktion: Pranay sent 0.8 ITSecCoin zu Miao"
t6 = "Transaktion: Lily sent 0.7 ITSecCoin zu Lea"

myblockchain = chain.chain()

myblockchain.create_block_from_transaction([t1, t2])
myblockchain.create_block_from_transaction([t3, t4])
myblockchain.create_block_from_transaction([t5, t6])

myblockchain.display_chain()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
