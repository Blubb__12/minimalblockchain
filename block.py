from hashlib import sha256
import json
import time

class block:
    def __init__(self, previous_block_hash, transaction_list):
        self.previous_block_hash = previous_block_hash
        self.nonce = 0
        #time only for teaching purposes needed
        self.time = 0
        self.transaction_list = transaction_list
        self.block_data = f"{' - '.join(transaction_list)} - {previous_block_hash}"
        self.block_hash = block.mine(self, 5)



    def compute_hash(self):
        block_string = json.dumps(self.__dict__, sort_keys=True)
        return str(sha256(block_string.encode() + bytes(self.nonce)).hexdigest())

    def mine(self, difficulty):
        blockhash = block.compute_hash(self)
        #start time measurement
        tic = time.perf_counter()
        while(blockhash[0:difficulty] != '0' * difficulty):
            self.nonce = self.nonce + 1
            blockhash = self.compute_hash()
        #stop time measurement
        toc = time.perf_counter()
        block.miningtime(self, tic, toc)
        return blockhash

    #get mining times as time property
    def miningtime(self, tic, toc):
            self.time = toc - tic